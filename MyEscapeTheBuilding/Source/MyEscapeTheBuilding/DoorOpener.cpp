#include "DoorOpener.h"
#include "Components/PrimitiveComponent.h"

// Constructor
UDoorOpener::UDoorOpener() {
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UDoorOpener::BeginPlay() {
	Super::BeginPlay();

	OwnerDoor = GetOwner();
}

// Called every frame
void UDoorOpener::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	((GetTotalMassInTriggerVolume() > TriggerMass) ? OnOpenRequest.Broadcast() : OnCloseRequest.Broadcast());
}

float UDoorOpener::GetTotalMassInTriggerVolume() {
	float TotalMass = 0.0f;

	for (const auto& TriggerVolume : TriggerVolumes) {
		
		if (TriggerVolume != nullptr) {
			TArray<AActor*> OverlappingActors;
			TriggerVolume->GetOverlappingActors(OUT OverlappingActors);

			for (const auto& Actor : OverlappingActors) {
				UPrimitiveComponent* PrimitiveComponent = Actor->FindComponentByClass<UPrimitiveComponent>();
				TotalMass += PrimitiveComponent->GetMass();
			}
		} else {
			UE_LOG(LogTemp, Error, TEXT("NO	TRIGGER VOLUME FOR: %s"), *(GetOwner()->GetName()))
		}

	}

	return TotalMass;
}