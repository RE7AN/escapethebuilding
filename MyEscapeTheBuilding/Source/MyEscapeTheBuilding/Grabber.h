#pragma once

#include "CoreMinimal.h"
#include "Components/InputComponent.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYESCAPETHEBUILDING_API UGrabber : public UActorComponent {
	GENERATED_BODY()

public:	
	UGrabber(); // Constructor
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override; // Called every frame

protected:
	virtual void BeginPlay() override; // Called when the game starts

private:
	float Reach = 100.0f;
	UInputComponent* InputComponent = nullptr;
	UPhysicsHandleComponent* PhysicsHandle = nullptr;


	void FindPhysicsHandle();
	void SetupInputComponent();

	void Grab(); // grab object in reach
	void Release(); // release held object

	FHitResult FindFirstPhysicsBodyInReach();
	void GetPlayerViewPoint(FVector& PVPLocation, FRotator& PVPRotation);
	void GetLineTracePoints(FVector& LineTraceBeginning, FVector& LineTraceEnd);

		
};
