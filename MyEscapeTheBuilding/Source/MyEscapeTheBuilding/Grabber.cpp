#include "Grabber.h"
#include "Components/ActorComponent.h" // for "InputComponent"
#include "Components/PrimitiveComponent.h" // for "UPrimitiveComponent"

#define OUT // just an indication that the parameter is of reference type

// Constructor
UGrabber::UGrabber() {
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UGrabber::BeginPlay() {
	Super::BeginPlay();

	FindPhysicsHandle();
	SetupInputComponent();
}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FVector LineTraceStart, LineTraceEnd;
	GetLineTracePoints(LineTraceStart, LineTraceEnd);

	// If physics handle is attached to an object
	if (PhysicsHandle->GrabbedComponent != nullptr)
		PhysicsHandle->SetTargetLocation(LineTraceEnd); // Reposition the object at the end of the line-trace
}

// Find - assumingly attached - physics handle
void UGrabber::FindPhysicsHandle() {
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle == nullptr) UE_LOG(LogTemp, Error, TEXT("%s has no physics handle"), *GetOwner()->GetName())
}
// Setup - assumingly attached - input component
void UGrabber::SetupInputComponent() {
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

	if (InputComponent != nullptr) {
		InputComponent->BindAction("Grab", EInputEvent::IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", EInputEvent::IE_Released, this, &UGrabber::Release);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("%s has no input component"), *GetOwner()->GetName());
	}
}


void UGrabber::GetPlayerViewPoint(FVector& PVPLocation, FRotator& PVPRotation) {
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT PVPLocation, OUT PVPRotation);
}
void UGrabber::GetLineTracePoints(FVector& LineTraceStart, FVector& LineTraceEnd) {
	FVector PVPLocation; FRotator PVPRotation;
	GetPlayerViewPoint(OUT PVPLocation, OUT PVPRotation);

	/// Line-trace points
	LineTraceStart = PVPLocation;
	LineTraceEnd = PVPLocation + PVPRotation.Vector() * Reach;
}

// Bound to 'F' and mouse right-click (Pressed)
void UGrabber::Grab() {
	/// Line-trace and see if any actors (with a physics body collision channel set) are reached
	FHitResult HitResult = FindFirstPhysicsBodyInReach();

	/// If an actor hits our line-trace
	if (HitResult.GetActor() != nullptr) {
		UPrimitiveComponent* ObjectInReach = HitResult.GetComponent();
		/// Attach physics handle
		PhysicsHandle->GrabComponentAtLocationWithRotation(
			ObjectInReach, // object to grab
			NAME_None, // no bones needed
			ObjectInReach->GetOwner()->GetActorLocation(), // grab location
			ObjectInReach->GetOwner()->GetActorRotation()  // grab rotation
		);
	}
}
// Bound to 'F' and mouse right-click (Released)
void UGrabber::Release() {
	/// (assuming physics handle was attached to an actor) detach physics handle
	if (PhysicsHandle != nullptr) {
		PhysicsHandle->ReleaseComponent(); // If the physics handle is not attached to anything this function will do nothing
	}
	else { // not necessary (handled internally)?
		UE_LOG(LogTemp, Warning, TEXT("Physics handle not attached to anything"))
	}
}

FHitResult UGrabber::FindFirstPhysicsBodyInReach() {
	/// Get line-trace points
	FVector LineTraceStart, LineTraceEnd;
	GetLineTracePoints(OUT LineTraceStart, OUT LineTraceEnd);

	/// Line-trace (AKA ray-cast) to find object in reach
	FHitResult HitResult;
	GetWorld()->LineTraceSingleByObjectType(
		OUT HitResult,
		LineTraceStart, LineTraceEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody), // types to collide with
		FCollisionQueryParams(FName(TEXT("")), false, GetOwner()) // more parameters about collisions
	);
	return HitResult;
}