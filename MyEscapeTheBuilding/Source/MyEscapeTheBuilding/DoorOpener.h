#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerVolume.h"
#include "Components/ActorComponent.h"
#include "DoorOpener.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYESCAPETHEBUILDING_API UDoorOpener : public UActorComponent {
	GENERATED_BODY()

public:	
	UDoorOpener(); // Constructor
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override; // Called every frame
	UPROPERTY(BlueprintAssignable) FDoorEvent OnOpenRequest; // Event of requesting the door to be opened
	UPROPERTY(BlueprintAssignable) FDoorEvent OnCloseRequest; // Event of requesting the door to be closed

protected:
	virtual void BeginPlay() override; // Called when the game starts

private:
	UPROPERTY(EditAnywhere) float TriggerMass = 15.0f; // minimum mass for the trigger volume to open the door

	AActor* OwnerDoor = nullptr;
	UPROPERTY(EditAnywhere) TArray<ATriggerVolume*> TriggerVolumes;

	float GetTotalMassInTriggerVolume();
		
};
