// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyEscapeTheBuildingGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYESCAPETHEBUILDING_API AMyEscapeTheBuildingGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
